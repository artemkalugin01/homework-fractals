﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1_Fractals
{
    class CloverFractal
    {
        public CloverFractal() : this(null) { }
        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="canvas">поле где будет происходить отрисовка</param>
        public CloverFractal(Control canvas) => this.Canvas = canvas;

        public Control Canvas { get; set; }
        public Color[] Colors = new Color[] { Color.Black,Color.Black, Color.Black, Color.Black, Color.Black
        ,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black};
        public int Direction { get; set; } = 1;
        public int Iterations { get; set; } = 0;

        /// <summary>
        /// внешний метод отрисовки из которого вызывается внутренний
        /// </summary>
        /// <param name="g"></param>
        public void DrawCloverFractal(Graphics g)
        {
            if (this.Canvas == null) return;
            PointF center = new PointF(this.Canvas.Width / 2.0f, this.Canvas.Height / 2.0f);
            float r = this.Canvas.Height * .162f;
            DrawCloverFractalinternal(g, center, r, this.Direction, this.Iterations);
        }

        /// <summary>
        /// внутренний метод отрисовки
        /// </summary>
        /// <param name="g">graphics</param>
        /// <param name="center">центр фрактала</param>
        /// <param name="r">радиус первого круга</param>
        /// <param name="dir">направление</param>
        /// <param name="iter">глкбина рекурсии</param>
        internal void DrawCloverFractalinternal(Graphics g, PointF center, float r, int dir, int iter)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            using (var brush = new SolidBrush(this.Colors[iter]))
            {
                g.FillEllipse(brush, center.X - r, center.Y - r, 2 * r, 2 * r);
            }

            if (iter == 0) return;
            float[] x = new float[4];
            float[] y = new float[4];
            float d = 3 * r / 2;
            x[0] = center.X - d;
            y[0] = center.Y;
            x[1] = center.X;
            y[1] = center.Y - d;
            x[2] = center.X + d;
            y[2] = center.Y;
            x[3] = center.X;
            y[3] = center.Y + d;

            for (int i = 0; i < 4; i++)
            {
                if (i - dir == 2 || i - dir == -2) continue;
                DrawCloverFractalinternal(g, new PointF(x[i], y[i]), r / 2, i, iter - 1);
            }
        }
    }
}
