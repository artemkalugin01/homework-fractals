﻿namespace _1_Fractals
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloverBox = new System.Windows.Forms.PictureBox();
            this.RecursionScroll = new System.Windows.Forms.TrackBar();
            this.LRecursionLevel = new System.Windows.Forms.Label();
            this.LDirection = new System.Windows.Forms.Label();
            this.DirectionScroll = new System.Windows.Forms.TrackBar();
            this.KantorBox = new System.Windows.Forms.PictureBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.nBox = new System.Windows.Forms.PictureBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.kochBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.startColorTextBox = new System.Windows.Forms.TextBox();
            this.endColorTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CloverBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecursionScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DirectionScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KantorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kochBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CloverBox
            // 
            this.CloverBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CloverBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CloverBox.Location = new System.Drawing.Point(194, 310);
            this.CloverBox.Name = "CloverBox";
            this.CloverBox.Size = new System.Drawing.Size(951, 551);
            this.CloverBox.TabIndex = 0;
            this.CloverBox.TabStop = false;
            this.CloverBox.Paint += new System.Windows.Forms.PaintEventHandler(this.Clover_Paint);
            // 
            // RecursionScroll
            // 
            this.RecursionScroll.Location = new System.Drawing.Point(331, 12);
            this.RecursionScroll.Maximum = 8;
            this.RecursionScroll.Minimum = 1;
            this.RecursionScroll.Name = "RecursionScroll";
            this.RecursionScroll.Size = new System.Drawing.Size(814, 90);
            this.RecursionScroll.TabIndex = 1;
            this.RecursionScroll.Value = 1;
            this.RecursionScroll.Scroll += new System.EventHandler(this.RecurScroll);
            // 
            // LRecursionLevel
            // 
            this.LRecursionLevel.AutoSize = true;
            this.LRecursionLevel.Location = new System.Drawing.Point(130, 12);
            this.LRecursionLevel.Name = "LRecursionLevel";
            this.LRecursionLevel.Size = new System.Drawing.Size(183, 25);
            this.LRecursionLevel.TabIndex = 2;
            this.LRecursionLevel.Text = "Глубина рекрсии";
            // 
            // LDirection
            // 
            this.LDirection.AutoSize = true;
            this.LDirection.Location = new System.Drawing.Point(162, 97);
            this.LDirection.Name = "LDirection";
            this.LDirection.Size = new System.Drawing.Size(146, 25);
            this.LDirection.TabIndex = 3;
            this.LDirection.Text = "Направление";
            // 
            // DirectionScroll
            // 
            this.DirectionScroll.Location = new System.Drawing.Point(331, 97);
            this.DirectionScroll.Maximum = 4;
            this.DirectionScroll.Minimum = 1;
            this.DirectionScroll.Name = "DirectionScroll";
            this.DirectionScroll.Size = new System.Drawing.Size(814, 90);
            this.DirectionScroll.TabIndex = 4;
            this.DirectionScroll.Value = 1;
            this.DirectionScroll.Scroll += new System.EventHandler(this.DirScroll);
            // 
            // KantorBox
            // 
            this.KantorBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.KantorBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KantorBox.Location = new System.Drawing.Point(194, 310);
            this.KantorBox.Name = "KantorBox";
            this.KantorBox.Size = new System.Drawing.Size(951, 551);
            this.KantorBox.TabIndex = 5;
            this.KantorBox.TabStop = false;
            this.KantorBox.Paint += new System.Windows.Forms.PaintEventHandler(this.Kantor_paint);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(87, 157);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(173, 29);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Квазиклевер";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.Clover_radioButton_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(87, 193);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(246, 29);
            this.radioButton2.TabIndex = 7;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Множество Кантора";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.Kantor_radioButton_CheckedChanged);
            // 
            // nBox
            // 
            this.nBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nBox.Location = new System.Drawing.Point(194, 310);
            this.nBox.Name = "nBox";
            this.nBox.Size = new System.Drawing.Size(951, 551);
            this.nBox.TabIndex = 8;
            this.nBox.TabStop = false;
            this.nBox.Paint += new System.Windows.Forms.PaintEventHandler(this.nBox_Paint);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(87, 229);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(151, 29);
            this.radioButton3.TabIndex = 9;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Н-фрактал";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.N_radioButton_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(87, 265);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(170, 29);
            this.radioButton4.TabIndex = 10;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Кривая Коха";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.Koch_radioButton_CheckedChanged);
            // 
            // kochBox
            // 
            this.kochBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kochBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.kochBox.Location = new System.Drawing.Point(194, 310);
            this.kochBox.Name = "kochBox";
            this.kochBox.Size = new System.Drawing.Size(951, 551);
            this.kochBox.TabIndex = 11;
            this.kochBox.TabStop = false;
            this.kochBox.Paint += new System.Windows.Forms.PaintEventHandler(this.kochBox_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 330);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 25);
            this.label1.TabIndex = 12;
            this.label1.Text = "start color";
            // 
            // startColorTextBox
            // 
            this.startColorTextBox.Location = new System.Drawing.Point(86, 432);
            this.startColorTextBox.Name = "startColorTextBox";
            this.startColorTextBox.Size = new System.Drawing.Size(100, 31);
            this.startColorTextBox.TabIndex = 13;
            this.startColorTextBox.Text = "#ffffff";
            // 
            // endColorTextBox
            // 
            this.endColorTextBox.Location = new System.Drawing.Point(86, 358);
            this.endColorTextBox.Name = "endColorTextBox";
            this.endColorTextBox.Size = new System.Drawing.Size(100, 31);
            this.endColorTextBox.TabIndex = 14;
            this.endColorTextBox.Text = "#000000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 404);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 25);
            this.label2.TabIndex = 15;
            this.label2.Text = "end color";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1157, 873);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.endColorTextBox);
            this.Controls.Add(this.startColorTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.kochBox);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.nBox);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.KantorBox);
            this.Controls.Add(this.DirectionScroll);
            this.Controls.Add(this.LDirection);
            this.Controls.Add(this.LRecursionLevel);
            this.Controls.Add(this.RecursionScroll);
            this.Controls.Add(this.CloverBox);
            this.Name = "Form1";
            this.Text = "Fractals";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.CloverBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecursionScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DirectionScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KantorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kochBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox CloverBox;
        private System.Windows.Forms.TrackBar RecursionScroll;
        private System.Windows.Forms.Label LRecursionLevel;
        private System.Windows.Forms.Label LDirection;
        private System.Windows.Forms.TrackBar DirectionScroll;
        private System.Windows.Forms.PictureBox KantorBox;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.PictureBox nBox;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.PictureBox kochBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox startColorTextBox;
        private System.Windows.Forms.TextBox endColorTextBox;
        private System.Windows.Forms.Label label2;
    }
}

