﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace _1_Fractals
{
    public partial class Form1 : Form
    {
        private CloverFractal cloverDrawing = null;
        private KantorFractal kantorDrawing = null;
        private NFractal nDrawing = null;
        private KohCurve kochDrawing = null;
        public Form1()
        {
            InitializeComponent();
            //инициализация объектов фракталов
            cloverDrawing = new CloverFractal(this.CloverBox);
            kantorDrawing = new KantorFractal(this.KantorBox);
            nDrawing = new NFractal(this.nBox);
            kochDrawing = new KohCurve(this.kochBox);
            

            //скрытие полей до выбора нужных
            CloverBox.Visible = false;
            KantorBox.Visible = false;
            DirectionScroll.Visible = false;
            LDirection.Visible = false;
            nBox.Visible = false;
            kochBox.Visible = false;





        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ChangeColors()
        {

        }

        /// <summary>
        /// метод скрола изменяющий глубину рекурсии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecurScroll(object sender, EventArgs e)
        {
            //передача значения всем объектам фракталов
            //TODO добавить для остальных фракталов
            cloverDrawing.Iterations = (sender as TrackBar).Value;
            kantorDrawing.Iterations = (sender as TrackBar).Value ;
            nDrawing.Iterations = (sender as TrackBar).Value + 1;
            kochDrawing.Iterations = (sender as TrackBar).Value;

            CheckColor();
            Color[] colors = new Color[(sender as TrackBar).Value+1];
            cloverDrawing.Colors = colors;
            CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                ColorTranslator.FromHtml(endColorTextBox.Text),cloverDrawing.Colors);
            nDrawing.Colors = colors;
            kochDrawing.Colors = colors;
            kantorDrawing.Colors = colors;

            //перерисовка изобрадения
            //(перерисовываются все так как неизвестно какой фрактал выбран, а скролл общий для всех)
            KantorBox.Invalidate();
            CloverBox.Invalidate();
            nBox.Invalidate();
            kochBox.Invalidate();
        }

        /// <summary>
        /// метод отрисовки квазиклевера на соответствующем picturebox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            CheckColor();
            if (cloverDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), cloverDrawing.Colors);
            }

            if (nDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), nDrawing.Colors);
            }
            if (kochDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), kochDrawing.Colors);
            }
            if (kantorDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), kantorDrawing.Colors);
            }

            CloverBox.Invalidate();
            KantorBox.Invalidate();
            nBox.Invalidate();
            kochBox.Invalidate();


        }

        /// <summary>
        /// метод скрола изменяющий направление квазиклевера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirScroll(object sender, EventArgs e)
        {
            //передача объекту клевера текущее значение скрола
            cloverDrawing.Direction = (sender as TrackBar).Value;
            CheckColor();
            if (cloverDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), cloverDrawing.Colors);
            }
            if (nDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), nDrawing.Colors);
            }
            if (kochDrawing != null)
            {
                CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                   ColorTranslator.FromHtml(endColorTextBox.Text), kochDrawing.Colors);
            }
            //перерисовка поля (picturebox)
            CloverBox.Invalidate();
        }

        /// <summary>
        /// метод отрисовки множества кантора на соответствующем поле
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Clover_Paint(object sender, PaintEventArgs e)
        {
            if (cloverDrawing == null) return;
            cloverDrawing.DrawCloverFractal(e.Graphics);
        }
        private void Kantor_paint(object sender, PaintEventArgs e)
        {
            if (kantorDrawing == null) return;
            kantorDrawing.DrawKantor(e.Graphics);

        }

        private void nBox_Paint(object sender, PaintEventArgs e)
        {
            if (cloverDrawing == null) return;
            nDrawing.Draw_N(e.Graphics);
        }
        private void kochBox_Paint(object sender, PaintEventArgs e)
        {
            if (cloverDrawing == null) return;
            kochDrawing.DrawKoh(e.Graphics);
        }

        private void Clover_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            CheckColor();
            Color[] colors = new Color[ RecursionScroll.Value + 1];
            cloverDrawing.Colors = colors;
            CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                ColorTranslator.FromHtml(endColorTextBox.Text), cloverDrawing.Colors);
            //убирает видимость панели множества кантора
            KantorBox.Visible = false;
            nBox.Visible = false;
            kochBox.Visible = false;

            //делает видимым панель для квазиклевера
            CloverBox.Visible = true;
            //делает видимым скрол с выбором направления
            DirectionScroll.Visible = true;
            //делает видимым подпись к скролу с выбором направления
            LDirection.Visible = true;




        }

        //кнопка выбора фрактала множество кантора
        private void Kantor_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            CheckColor();
            CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                  ColorTranslator.FromHtml(endColorTextBox.Text), kantorDrawing.Colors);
            //убирает видимость панели выводящей квазиклевер
            CloverBox.Visible = false;
            nBox.Visible = false;
            kochBox.Visible = false;
            //убирает скролл выбора направления так как не используется для этого фрактала
            DirectionScroll.Visible = false;
            //убирает подпись к скролу выбора направления
            LDirection.Visible = false;

            //делает видимым панели для отрисовки множества кантора
            KantorBox.Visible = true;



        }

        private void N_radioButton_CheckedChanged(object sender, EventArgs e)
        {

            CheckColor();
            Color[] colors = new Color[RecursionScroll.Value + 1];
            cloverDrawing.Colors = colors;
            CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                ColorTranslator.FromHtml(endColorTextBox.Text), nDrawing.Colors);
            CloverBox.Visible = false;
            KantorBox.Visible = false;
            kochBox.Visible = false;

            DirectionScroll.Visible = false;
            LDirection.Visible = false;


            nBox.Visible = true;

        }

        private void Koch_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            CheckColor();
            CalculateColors(ColorTranslator.FromHtml(startColorTextBox.Text),
                  ColorTranslator.FromHtml(endColorTextBox.Text), kochDrawing.Colors);
            CloverBox.Visible = false;
            KantorBox.Visible = false;
            nBox.Visible = false;

            DirectionScroll.Visible = false;
            LDirection.Visible = false;

            kochBox.Visible = true;
        }


        public  void CheckColor()
        {
            try
            {
                Color startColor = ColorTranslator.FromHtml(startColorTextBox.Text);
                Color endColor = ColorTranslator.FromHtml(endColorTextBox.Text);
            }
            catch(Exception)
            {
                MessageBox.Show("Неверно задан цвет.\nФормат ввода :#ffffff в шестнадцатиричной");
                startColorTextBox.Text = "#ffffff";
                endColorTextBox.Text = "#000000";
            }
            
        }

        public void CalculateColors(Color startColor,Color endColor,Color[] colors)
        {
            int rMin = startColor.R;
            int gMin = startColor.G;
            int bMin = startColor.B;

            int rMax = endColor.R;
            int gMax = endColor.G;
            int bMax = endColor.B;


            int size = colors.Length;
            for (int i = size-1; i > 0; i--)
            {
                var rAverage = rMin + (int)((rMax - rMin) * i / size);
                var gAverage = gMin + (int)((gMax - gMin) * i / size);
                var bAverage = bMin + (int)((bMax - bMin) * i / size);
                colors[i] = Color.FromArgb(rAverage, gAverage, bAverage);
            }

           
        }
    }
}

