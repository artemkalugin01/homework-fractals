﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1_Fractals
{
    class KohCurve
    {
        public KohCurve() : this(null) { }
        public KohCurve(Control canvas) => this.Canvas = canvas;
        public Control Canvas { get; set; }

        public Color[] Colors = new Color[] { Color.Black,Color.Black, Color.Black, Color.Black, Color.Black
        ,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black};

        public Color Color { get; set; } = Color.Black;
        public int Iterations { get; set; } = 1;
        /// <summary>
        /// Метод определяет исходный треугольник и вызывает
        /// </summary>
        /// <param name="g"></param>
        public void DrawKoh(Graphics g)
        {
            if (this.Canvas == null) return;
            Color deleteColor = Canvas.BackColor;
            var pen1 = new Pen(Colors[0], 2);
            var pen2 = new Pen(deleteColor, 2);
            //Определим координаты исходного треугольника
            var point1 = new PointF(0, Canvas.Height - Canvas.Height / 20);
            var point2 = new PointF(Canvas.Width, Canvas.Height - Canvas.Height / 20);
            var point3 = new PointF(Canvas.Width / 2, Canvas.Height * 2);
            g.DrawLine(pen1, point1, point2);
            DrawKohInternal(point1, point2, point3, pen1, pen2, Iterations, g);
        }
        /// <summary>
        /// Метод рисует заданный фрактал.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="pen1"></param>
        /// <param name="deletePen"></param>
        /// <param name="iter"></param>
        /// <param name="g"></param>
        void DrawKohInternal(PointF p1, PointF p2, PointF p3, Pen pen1, Pen deletePen, int iter, Graphics g)
        {
            if (iter == 0) return;
            pen1.Color = Colors[iter];
            var p4 = new PointF((p2.X + 2 * p1.X) / 3, (p2.Y + 2 * p1.Y) / 3);
            var p5 = new PointF((2 * p2.X + p1.X) / 3, (p1.Y + 2 * p2.Y) / 3);
            var ps = new PointF((p2.X + p1.X) / 2, (p2.Y + p1.Y) / 2);
            var pn = new PointF((4 * ps.X - p3.X) / 3, (4 * ps.Y - p3.Y) / 3);
            g.DrawLine(pen1, p4, pn);
            g.DrawLine(pen1, p5, pn);
            g.DrawLine(deletePen, p4, p5);
            DrawKohInternal(p4, pn, p5, pen1, deletePen, iter - 1, g);
            DrawKohInternal(pn, p5, p4, pen1, deletePen, iter - 1, g);
            DrawKohInternal(p1, p4, new PointF((2 * p1.X + p3.X) / 3, (2 * p1.Y + p3.Y) / 3), pen1, deletePen, iter - 1, g);
            DrawKohInternal(p5, p2, new PointF((2 * p2.X + p3.X) / 3, (2 * p2.Y + p3.Y) / 3), pen1, deletePen, iter - 1, g);
        }
    }
}
