﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace _1_Fractals
{
    class KantorFractal
    {
        public KantorFractal() : this(null) { }
        public KantorFractal(Control canvas) => this.Canvas = canvas;
        public Control Canvas { get; set; }
        public Color Color { get; set; } = Color.Black;
        public Color[] Colors = new Color[] { Color.Black,Color.Black, Color.Black, Color.Black, Color.Black
        ,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black};

        public int Iterations { get; set; } = 1;

        /// <summary>
        /// внешний метод отрисовки из которого вызывается внутренний
        /// </summary>
        /// <param name="g"></param>
        public void DrawKantor(Graphics g)
        {
            if (this.Canvas == null) return;
            DrawKantorInternal(0, 0, Canvas.Width, Iterations, g);
        }

        /// <summary>
        ///  внутренний метод отрисовки
        /// </summary>
        /// <param name="x">точка начала по X</param>
        /// <param name="y">точка начала по Y</param>
        /// <param name="width">ширина первого прямоугольника</param>
        /// <param name="recursion">глубина рекурсии</param>
        /// <param name="g">graphics</param>
        public void DrawKantorInternal(float x,float y,float width, int recursion, Graphics g)
        {

            if (recursion == 0) { return; }
           
            //y1 equals to y2 here
            using (var brush = new SolidBrush(Colors[recursion]))
            {
                g.FillRectangle(brush, new RectangleF(x, y, width,Canvas.Height/20));
            }

            //отступ по y
            y += Canvas.Height / 20;
            //рекурсивный вызов с новыми координатами и recursion-=1
            DrawKantorInternal(x,y+Canvas.Height/20,width/3,recursion-1,g);
            DrawKantorInternal(x + width * 2 / 3, y+Canvas.Height / 20, width / 3,recursion-1,g);
        }
    }
}
