﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1_Fractals
{
    class NFractal
    {
        public NFractal() : this(null) { }
        public NFractal(Control canvas) => this.Canvas = canvas;

        public Control Canvas { get; set; }
        public Color[] Colors = new Color[] { Color.Black,Color.Black, Color.Black, Color.Black, Color.Black
        ,Color.Black,Color.Black,Color.Black,Color.Black,Color.Black};
        public int Iterations { get; set; } = 1;

        public void Draw_N(Graphics g)
        {
            if (this.Canvas == null) return;
            Draw_N_Internal(Canvas.Width / 2, Canvas.Height / 2, (float)(Canvas.Width / 8), Iterations, g);

        }

        public void Draw_N_Internal(float x, float y, float width, int recursion, Graphics g)
        {
            if (recursion == 0) { return; }
            float x1 = x + width;
            float x2 = x - width;
            float y1 = y + width;
            float y2 = y - width;

            //y1 equals to y2 here
            using (var pen = new Pen(Colors[recursion-1]))
            {
                g.DrawLine(pen, x, y, x1, y);
                g.DrawLine(pen, x, y, x2, y);

                g.DrawLine(pen, x1, y, x1, y1);
                g.DrawLine(pen, x1, y, x1, y2);

                g.DrawLine(pen, x2, y, x - width, y1);
                g.DrawLine(pen, x2, y, x - width, y2);
            }

            Draw_N_Internal(x - width, y1, width / 2, recursion - 1, g);
            Draw_N_Internal(x + width, y1, width / 2, recursion - 1, g);
            Draw_N_Internal(x - width, y2, width / 2, recursion - 1, g);
            Draw_N_Internal(x + width, y2, width / 2, recursion - 1, g);
        }
    }
}
